import React from 'react'
import { Helmet } from 'react-helmet'
import { Box, Flex, Heading, Link, Text } from 'rebass'
import ProjectCard from './components/ProjectCard'

const AndMore = () => {
  return (
    <Text alignSelf="center" mx={2} color="white" textAlign="center">
      and more...
    </Text>
  )
}

const Home = () => {
  return (
    <>
      <Helmet>
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/favicons/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicons/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicons/favicon-16x16.png"
        />
        <link rel="manifest" href="/favicons/site.webmanifest" />
        <link
          rel="mask-icon"
          href="/favicons/safari-pinned-tab.svg"
          color="#000000"
        />
        <meta name="msapplication-TileColor" content="#000000" />
        <meta name="theme-color" content="#ffffff" />

        <link rel="alternate" type="application/rss+xml" href="/feed.xml" />

        <meta
          name="description"
          content="Zane Helton's portfolio of open and closed source projects."
        />
        <title>Zane's Portfolio</title>
      </Helmet>
      <Flex
        px={5}
        maxWidth={['100%', '100%', '1280px']}
        marginLeft="auto"
        marginRight={['auto', 'auto', '200px']}
      >
        <Box width={1}>
          <Box mt={5}>
            <Link href="https://zaaane.com" color="white">
              &larr; Return to zaaane.com
            </Link>
          </Box>
          <Heading
            variant="h2"
            textAlign="left"
            paddingBottom={2}
            paddingTop={4}
            fontFamily="'Alkatra', cursive"
          >
            Projects
          </Heading>
          <Flex
            flexWrap="wrap"
            mx={-2}
            flexDirection={['column', 'row', 'row']}
          >
            {/* Pinned */}
            <ProjectCard
              githubURL="https://github.com/ZaneH-blog/"
              websiteURL="https://zaaane.com/blog"
              thumbnailSrc="/img/thumbnails/blog.png"
              description="Blog about what I've been doing and learning."
              year="2015-2024"
              detailsPosition="bottom"
            >
              Blog
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/dafarmz-dc.png"
              websiteURL="https://dafarmz.zaaane.com"
              description="Discord chat-based farming game."
              blogURL="https://zaaane.com/blog/generating-images-for-a-discord-game"
              githubURL="https://github.com/ZaneH/dafarmz-bot"
              year="2023"
              detailsPosition="bottom"
            >
              DaFarmz
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/color-chai-dc.png"
              githubURL="https://github.com/ZaneH/color-chai"
              description="Tiny Discord bot that posts images of color codes."
              year="2023"
              detailsPosition="bottom"
            >
              Color Chai
            </ProjectCard>
            <ProjectCard
              githubURL="https://github.com/ZaneH/ocw-crawler"
              description="Crawls MIT OpenCourseWare and exports a CSV of courses with video lectures."
              noThumbnail
              year="2023"
              detailsPosition="bottom"
            >
              ocw-crawler
            </ProjectCard>
            <ProjectCard
              seeFullThumbnail
              githubURL="https://github.com/ZaneH/skywatch"
              thumbnailSrc="/img/thumbnails/skywatch.png"
              description="METAR (aviation weather) decoder in SwiftUI. Supports iPad and macOS."
              year="2023"
              detailsPosition="bottom"
            >
              Skywatch
            </ProjectCard>
            <ProjectCard
              videoSrc="/vid/roblox-mancala.mp4"
              blogURL="https://zaaane.com/blog/revisiting-roblox"
              websiteURL="https://www.roblox.com/games/13457986000/Mancala"
              description="Roblox port of Mancala. Available to play and edit for free."
              year="2023"
              detailsPosition="bottom"
            >
              Mancala
            </ProjectCard>
            <ProjectCard
              noThumbnail
              githubURL="https://github.com/ZaneH/exchange-rate-api"
              description="Simple API for getting live exchange rates."
              year="2023"
              detailsPosition="bottom"
            >
              Exchange Rate API
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/piano-trainer.png"
              githubURL="https://github.com/ZaneH/scale-trainer/"
              websiteURL="https://zaneh.itch.io/piano-trainer"
              blogURL="https://zaaane.com/blog/piano-trainer-was-a-success"
              description="Cross-platform tool for learning the piano. Supports MIDI and QWERTY input."
              videoURL="https://vimeo.com/730642802"
              year="2022-2023"
              detailsPosition="bottom"
            >
              Piano Trainer
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/metronome.png"
              githubURL="https://github.com/ZaneH/metronome/"
              websiteURL="https://zaaane.com/metronome"
              description="Cross-platform metronome. Has keyboard shortcuts, themes, and more."
              seeFullThumbnail
              year="2022"
              detailsPosition="bottom"
            >
              Metronome
            </ProjectCard>
            <ProjectCard
              videoSrc="/vid/zabo-demo.mp4"
              githubURL="https://github.com/ZaneH/zabo-demo"
              description="Portfolio demo using the Zabo SDK. View the balance of any Ethereum wallet and the balance/transactions of connected wallets."
              year="2021"
              detailsPosition="bottom"
            >
              zabo-demo
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/wavr-discord-bot.png"
              githubURL="https://github.com/ZaneH/wavr-discord-bot/"
              description="Discord bot for the Wavr digital marketplace. Supports embeds and vote rewards."
              year="2020"
              detailsPosition="bottom"
            >
              wavr-discord-bot
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/wavr-home.png"
              githubURL="https://github.com/ZaneH/wavr/"
              description="Digital marketplace complete with PayPal, promo codes, and more. Solo project during COVID-19."
              year="2020"
              detailsPosition="bottom"
            >
              Wavr
            </ProjectCard>
            <ProjectCard
              videoSrc="/vid/blare.mp4"
              description="Mobile music streaming platform with a focus on discovery. Founded + built with friends."
              year="2018-2020"
              detailsPosition="bottom"
            >
              Blare
            </ProjectCard>
            <ProjectCard
              seeFullThumbnail
              videoSrc="/vid/flutter-pm.mp4"
              githubURL="https://github.com/ZaneH/Flutter_PM-Mockup"
              description="Flutter demo based on a Dribbble shot."
              year="2019"
            >
              Flutter Property Management
            </ProjectCard>
            <ProjectCard
              seeFullThumbnail
              videoSrc="/vid/flutter-cards.mp4"
              githubURL="https://github.com/ZaneH/Flutter_Cards-Mockup"
              description="Flutter demo based on a Dribbble shot."
              year="2019"
            >
              Flutter Cards
            </ProjectCard>
            <ProjectCard
              seeFullThumbnail
              videoSrc="/vid/techshop.mp4"
              githubURL="https://github.com/ZaneH/Flutter_TechShop-Mockup"
              description="Flutter demo based on a Dribbble shot."
              year="2019"
            >
              Flutter Tech Shop
            </ProjectCard>
            <ProjectCard
              seeFullThumbnail
              videoSrc="/vid/petstore.mp4"
              githubURL="https://github.com/ZaneH/Flutter_PetStore-Mockup"
              description="Flutter demo based on a Dribbble shot."
              year="2019"
            >
              Flutter Pet Store
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/genetic-gradient-v2.png"
              githubURL="https://github.com/ZaneH/genetic-gradient-v2/"
              description="Genetic algorithm used to find the center of any gradient."
              year="2016"
            >
              genetic-gradient-v2
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/cctimercountdown.png"
              githubURL="https://github.com/ZaneH/CCTimerCountdown"
              description="iOS tweak to display the remaining time on a Timer within the iOS control center."
              year="2015-2018"
            >
              CCTimerCountdown
            </ProjectCard>
            <ProjectCard
              githubURL="https://github.com/ZaneH/hot-as-balls-tweak/"
              thumbnailSrc="/img/thumbnails/hot-as-balls.png"
              description="Gag tweak that let's you know if it's hot or cold as balls outside."
              year="2016"
            >
              Hot as Balls
            </ProjectCard>
            <ProjectCard
              videoURL="https://youtu.be/FS4eOLgHlXs"
              thumbnailSrc="/img/thumbnails/game-forger.jpeg"
              websiteURL="https://www.producthunt.com/products/game-forger"
              description="iOS application for developing games straight from your device. Built with friends."
              year="2015-2016"
            >
              Game Forger
            </ProjectCard>
            <ProjectCard
              githubURL="https://github.com/ZaneH/unsplashwallpaper"
              description="iOS tweak to set your wallpaper to a random image from Unsplash.com. Integrates with Activator by rpetrich."
              year="2015"
            >
              UnsplashWallpaper
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="https://i.imgur.com/KBA4Ow5.png"
              description="iOS app and SDK to quickly check the price of Omnicoin."
              year="2015"
            >
              Omnicoin
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/chineseskill-hack.jpeg"
              githubURL="https://github.com/ZaneH/ChineseSkill-Hack"
              description="iOS tweak that adds cheats to the ChineseSkill application."
              year="2015"
            >
              ChineseSkill-Hack
            </ProjectCard>
            <ProjectCard
              videoURL="https://youtu.be/RZXFANNLV2U"
              thumbnailSrc="/img/thumbnails/circularis.jpeg"
              websiteURL="https://zaaane.com/circularis/index.html"
              description="iOS game based on simple mathematics. Clear all of the circles to win. The linked demo is an experimental rewrite for web. Built with friends."
              year="2014-2015"
            >
              Circularis
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/space-gravity.jpeg"
              websiteURL="https://zaaane.com/space-gravity/index.html"
              description="iOS game that's similar to Asteroids. The linked demo is an experimental rewrite for web. Built with friends."
              year="2014-2015"
            >
              Space Gravity
            </ProjectCard>
            <ProjectCard
              seeFullThumbnail
              thumbnailSrc="/img/thumbnails/firetv-remote.png"
              githubURL="https://github.com/ZaneH/FireTVRemote-Node"
              description="Web app that connects to your FireTV and acts as a remote. Uses ADB in the background."
              year="2015"
            >
              FireTVRemote-Node
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/synctube.png"
              githubURL="https://github.com/ZaneH/synctube"
              description="Web app utilizing socket.io to play YouTube videos in sync across computers and networks."
              seeFullThumbnail
              year="2015"
            >
              SyncTube
            </ProjectCard>
            <ProjectCard
              videoSrc="/vid/bswquicktype.mp4"
              githubURL="https://github.com/BisonSoftware/BSWQuickType"
              description="iOS component that replicates Apple's QuickType keyboard but with custom text."
              seeFullThumbnail
              year="2015"
            >
              BSWQuickType
            </ProjectCard>
            <ProjectCard
              noThumbnail
              sourceURL="https://bitbucket.org/ZaneH/portfolio2/src/master/"
              description="This website's code. I host it on Bitbucket in hopes that it will be less likely to be forked."
              year="2023"
            >
              This Portfolio
            </ProjectCard>
            <AndMore />
          </Flex>
        </Box>
      </Flex>
      <Flex
        px={5}
        maxWidth={['100%', '100%', '1280px']}
        marginLeft="auto"
        marginRight={['auto', 'auto', '200px']}
      >
        <Box width={1}>
          <Heading
            variant="h2"
            textAlign="left"
            paddingBottom={2}
            paddingTop={4}
          >
            Video Tutorials
          </Heading>
          <Flex
            flexWrap="wrap"
            mx={-2}
            flexDirection={['column', 'row', 'row']}
          >
            <ProjectCard
              thumbnailSrc="/img/thumbnails/elixir-series-yt.jpg"
              websiteURL="https://www.youtube.com/watch?v=Vz7qH_dJQC0&list=PLFWEDfSyl7h88ZgEIkLdf8xI6XBBhNBkc"
              sourceURL="https://github.com/ZaneH-YT/.github/blob/main/profile/README.md"
              description="Tutorial series for learning Elixir... quickly."
              detailsPosition="top"
              year="2023"
              shouldGoUp
            >
              Learn Elixir Quickly
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/tweak-series-yt.jpg"
              websiteURL="https://www.youtube.com/watch?v=uNXd4KLLjhk&list=PLFWEDfSyl7h_K8Ew4rwTzlUPgWU7nKYri"
              sourceURL="https://github.com/ZaneH/Tweak-Series"
              description="Tutorial series for developing jailbreak tweaks on iOS."
              detailsPosition="top"
              year="2018"
              shouldGoUp
            >
              iOS Tweak Development
            </ProjectCard>
          </Flex>
        </Box>
      </Flex>
      <Flex
        px={5}
        maxWidth={['100%', '100%', '1280px']}
        marginLeft="auto"
        marginRight={['auto', 'auto', '200px']}
      >
        <Box width={1}>
          <Heading
            variant="h2"
            textAlign="left"
            paddingBottom={2}
            paddingTop={4}
          >
            Contracted
          </Heading>
          <Flex
            flexWrap="wrap"
            mx={-2}
            flexDirection={['column', 'row', 'row']}
          >
            <ProjectCard
              noThumbnail
              websiteURL="https://streamerdash.com/landing"
              sourceURL="https://docs.streamerdash.com/"
              description="Dashboard for live streamers. Merges chat, reads out donations, and more."
              year="2023"
              detailsPosition="top"
              shouldGoUp
            >
              Streamaze
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="/img/thumbnails/business-calc.png"
              websiteURL="https://exitally.com"
              description="Modern business valuation calculator for the web."
              year="2022"
              shouldGoUp
            >
              Exitally
            </ProjectCard>
            <ProjectCard
              seeFullThumbnail
              videoSrc="/vid/asteroid.mp4"
              description={`Logo removed for privacy. Theme: Pixel art inspired loops. [5/20]`}
              year="2020"
              detailsPosition="top"
              shouldGoUp
            >
              Asteroids
            </ProjectCard>
            <ProjectCard
              seeFullThumbnail
              videoSrc="/vid/moon-water.mp4"
              description={`Logo removed for privacy. Theme: Pixel art inspired loops. [2/20]`}
              year="2020"
              detailsPosition="top"
              shouldGoUp
            >
              Moon Water
            </ProjectCard>
            {/* <ProjectCard
              seeFullThumbnail
              videoSrc="/vid/green-shard.mp4"
              description="Theme: Rotating green shard surrounded by smaller shards."
              year="2020"
              detailsPosition="top"
              shouldGoUp
            >
              Green Shard
            </ProjectCard> */}
            <ProjectCard
              videoSrc="/vid/knightvpn.mp4"
              description="Feature-rich VPN client for Windows based on OpenVPN."
              videoURL="https://vimeo.com/259611450"
              year="2014"
              detailsPosition="top"
              shouldGoUp
            >
              KnightVPN (Windows)
            </ProjectCard>
            <ProjectCard
              thumbnailSrc="https://i.imgur.com/GqXMF7f.png"
              description="VPN client for iOS. First solo iOS launch."
              year="2014"
              detailsPosition="top"
              shouldGoUp
            >
              KnightVPN (iOS)
            </ProjectCard>
            <AndMore />
          </Flex>
        </Box>
      </Flex>
      {/* <Flex
        px={5}
        maxWidth={['100%', '100%', '1280px']}
        marginLeft="auto"
        marginRight={['auto', 'auto', '200px']}
      >
        <Box width={1}>
          <Heading
            variant="h2"
            textAlign="left"
            paddingBottom={2}
            paddingTop={4}
          >
            Media
          </Heading>
          <Flex
            flexWrap="wrap"
            mx={-2}
            flexDirection={['column', 'row', 'row']}
          >
            <ProjectCard
              seeFullThumbnail
              videoSrc="/vid/portal.mp4"
              description="Just for fun. Inspired by Osean World."
              detailsPosition="top"
              year="2020"
              shouldGoUp
            >
              Portal
            </ProjectCard>
            <ProjectCard
              shouldGoUp
              seeFullThumbnail
              videoSrc="/vid/isopod.webm"
              videoType="video/webm"
              description="Just for fun. Includes custom assets and free assets from online."
              detailsPosition="top"
              year="2020"
            >
              Isopod
            </ProjectCard>
            <ProjectCard
              shouldGoUp
              seeFullThumbnail
              videoSrc="/vid/crystal-skull.mp4"
              description="Just for fun. Inspired by Osean World."
              detailsPosition="top"
              year="2020"
            >
              Crystal Skull
            </ProjectCard>
            <ProjectCard
              shouldGoUp
              seeFullThumbnail
              videoSrc="/vid/iridescence.mp4"
              description="Just for fun. I mean, just look at it!"
              detailsPosition="top"
              year="2020"
            >
              Iridescence
            </ProjectCard>
            <ProjectCard
              shouldGoUp
              seeFullThumbnail
              videoSrc="/vid/ice-cream.mp4"
              description="Just for fun."
              detailsPosition="top"
              year="2020"
            >
              Ice Cream
            </ProjectCard>
            <ProjectCard
              shouldGoUp
              videoSrc="/vid/space-guy.mp4"
              description={`Just for fun. Experiment with custom character models and "clothing."`}
              detailsPosition="top"
              year="2020"
            >
              Space Guy
            </ProjectCard>
            <ProjectCard
              shouldGoUp
              seeFullThumbnail
              videoSrc="/vid/torch.mp4"
              description="Just for fun."
              detailsPosition="top"
              year="2020"
            >
              Torch
            </ProjectCard>
            <ProjectCard
              shouldGoUp
              thumbnailSrc="/img/thumbnails/toilet-paper.png"
              description="Just for fun. Made during the toilet paper crisis of 2020. Inspired by an article I saw."
              detailsPosition="top"
              year="2020"
            >
              Toilet Paper
            </ProjectCard>
            <AndMore />
          </Flex>
        </Box>
      </Flex> */}
    </>
  )
}

export default Home
