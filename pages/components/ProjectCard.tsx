import React from 'react'
import { BiGlobe } from 'react-icons/bi'
import { FaCode, FaFilm, FaGithub, FaPenNib } from 'react-icons/fa'
import { Box, Image, Link, Text } from 'rebass'
import IconLink from './IconLink'

interface Props {
  children: any
  description: string
  seeFullThumbnail?: boolean
  thumbnailSrc?: string
  videoSrc?: string
  videoType?: string
  githubURL?: string
  sourceURL?: string
  websiteURL?: string
  blogURL?: string
  videoURL?: string
  noThumbnail?: boolean
  year?: string
  detailsPosition?: 'top' | 'bottom'
  shouldGoUp?: boolean
}

const ProjectCard = ({
  children,
  thumbnailSrc,
  videoSrc,
  githubURL,
  websiteURL,
  blogURL,
  sourceURL,
  seeFullThumbnail,
  description,
  videoURL,
  noThumbnail,
  year,
  detailsPosition,
  shouldGoUp,
  videoType = 'video/mp4',
}: Props) => {
  const transformValue =
    detailsPosition === 'top' ? 'translateY(calc(-100% - 2rem))' : ''

  return (
    <Box
      my={2}
      mx={2}
      width={['max-content', 'auto', 'auto']}
      sx={{
        backgroundColor: 'rgb(190, 190, 190)',
        boxSizing: 'border-box',
        transition: 'all 0.1s ease-in-out',
        '&:hover': {
          backgroundColor: 'white',
          outline: '4px #000 solid',
          '.details-dropdown': {
            opacity: 1,
            pointerEvents: 'all',
          },
        },
      }}
    >
      <Link
        target="_blank"
        href={websiteURL ?? githubURL ?? videoURL ?? blogURL ?? sourceURL}
        sx={{
          cursor:
            websiteURL || githubURL || videoURL || blogURL || sourceURL
              ? 'pointer'
              : 'crosshair',
        }}
      >
        <Box px={3} py={1}>
          <Text>{children}</Text>
        </Box>
      </Link>
      <Box
        className="details-dropdown"
        maxWidth="-webkit-fill-available"
        width="400px"
        py={2}
        sx={{
          transition: 'all 0.1s ease-in-out',
          transform: [
            detailsPosition !== 'bottom' && 'translateY(calc(-100% - 2rem))',
            shouldGoUp ? transformValue : 'none',
            shouldGoUp ? transformValue : 'none',
          ],
          position: 'absolute',
          opacity: 0,
          pointerEvents: 'none',
          '&:hover': {
            display: 'block',
          },
        }}
      >
        <Box py={3} px={3} bg="white" sx={{ outline: '4px #000 solid' }}>
          <Text fontWeight="bold">{year}</Text>
          {description}
          <br />
          {websiteURL && (
            <IconLink
              url={websiteURL}
              icon={<BiGlobe className="link-icon" />}
            />
          )}
          {githubURL && (
            <IconLink
              url={githubURL}
              icon={<FaGithub className="link-icon" />}
            />
          )}
          {videoURL && (
            <IconLink url={videoURL} icon={<FaFilm className="link-icon" />} />
          )}
          {blogURL && (
            <IconLink url={blogURL} icon={<FaPenNib className="link-icon" />} />
          )}
          {sourceURL && (
            <IconLink url={sourceURL} icon={<FaCode className="link-icon" />} />
          )}
          {websiteURL || githubURL || videoURL || blogURL || sourceURL ? (
            <br />
          ) : null}
          {!noThumbnail && !videoSrc && thumbnailSrc && (
            <Image
              mt={3}
              mx="auto"
              maxHeight={['300px', '300px', '40vh']}
              variant="thumbnail"
              className="thumbnail"
              src={thumbnailSrc}
              sx={{ objectFit: seeFullThumbnail ? 'contain' : 'cover' }}
            />
          )}
          {videoSrc && (
            <Box mt={3}>
              <video
                controls
                style={{
                  maxWidth: '100%',
                  borderRadius: '0.25rem',
                  maxHeight: '40vh',
                }}
                autoPlay
                muted
                loop
                playsInline
              >
                <source src={videoSrc} type={videoType} />
                Your browser does not support the video tag.
              </video>
            </Box>
          )}
        </Box>
      </Box>
    </Box>
  )
}

export default ProjectCard
