import React from 'react'
import { Box, Link } from 'rebass'

interface Props {
  icon: any
  url?: string
}

const IconLink = ({ icon, url }: Props) => {
  return (
    <Link
      mt={3}
      display="inline-block"
      href={url || '#'}
      target={url ? '_blank' : '_self'}
    >
      <Box
        variant="iconLink"
        mx={1}
        sx={{
          textAlign: 'center',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {icon}
      </Box>
    </Link>
  )
}

export default IconLink
