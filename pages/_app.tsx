import { ThemeProvider } from 'theme-ui'
import '../styles/globals.css'
import preset from '@rebass/preset'
import React from 'react'
import { Helmet } from 'react-helmet'

const MyApp = ({ Component, pageProps }) => {
  const theme = {
    ...preset,
    colors: {
      background: '#030303',
      white: '#FFFFFC',
      clear: 'rgba(0, 0, 0, 0)',
      text: '#000',
    },
    buttons: {
      link: {
        px: 2,
        py: 2,
        borderRadius: 2,
        cursor: 'pointer',
      },
    },
    text: {
      h2: {
        color: 'white',
        fontSize: '2.3rem',
        fontFamily: "'Alkatra', cursive",
      },
    },
    variants: {
      card: {
        bg: 'white',
        borderRadius: 'default',
        border: '3px solid white',
      },
      thumbnail: {
        borderRadius: 'default',
      },
      iconLink: {
        bg: 'background',
        color: 'white',
        width: '28px',
        height: '28px',
        borderRadius: 'default',
        marginLeft: 2,
      },
    },
    radii: {
      default: '0.25rem',
      circle: 99999,
    },
    fonts: {
      body: 'Amori, sans-serif',
      heading: 'Amori, sans-serif',
    },
  }

  return (
    <ThemeProvider theme={theme}>
      <Helmet>
        <title>Zane's Portfolio</title>
      </Helmet>
      <Component {...pageProps} />
    </ThemeProvider>
  )
}

export default MyApp
