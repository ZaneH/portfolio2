const { METRONOME_URL, BLOG_URL } = process.env

/** @type {import('next').NextConfig} */
module.exports = {
  async rewrites() {
    return [
      {
        source: '/:path*',
        destination: `/:path*`,
      },
      {
        source: '/metronome',
        destination: `${METRONOME_URL}/metronome`,
      },
      {
        source: '/metronome/:path*',
        destination: `${METRONOME_URL}/metronome/:path*`,
      },
      {
        source: '/blog',
        destination: `${BLOG_URL}/blog`,
      },
      {
        source: '/blog/:path*',
        destination: `${BLOG_URL}/blog/:path*`,
      },
      {
        source: '/resume',
        destination: `/resume.pdf`,
      },
    ]
  },
}
