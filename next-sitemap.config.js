/** @type {import('next-sitemap').IConfig} */
module.exports = {
  siteUrl: process.env.SITE_URL || 'https://portfolio.zaaane.com',
  exclude: ['/components/*'],
  generateRobotsTxt: true,
  generateIndexSitemap: false,
  additionalPaths: async (config) => {
    const result = []
    result.push({
      loc: 'https://zaaane.com',
      changefreq: 'monthly',
      priority: 0.7,
      lastmod: new Date().toISOString()
    })

    return result
  }
}
